# Script Sharepoint Instructions

### What is this repository for? ###

* Quick summary

This is a wiki for TAs on how to add content to the script.byu.edu website

* [Current Website](https://script.byu.edu/Pages/home.aspx)

* Note: This is public for Professor and TA reference. Web developers please use the instructions in the private wiki for back-end notes.

##Go to [wiki](https://bitbucket.org/fhsscfhg/scriptwiki/wiki/Home)